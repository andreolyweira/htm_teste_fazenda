namespace HTM_Teste_Fazenda.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Criando_Base : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FazendaNewEditVM",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                        Sigla = c.String(),
                        IdProprietario = c.Int(nullable: false),
                        IdMunicipio = c.Int(nullable: false),
                        Ativo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FazendaNewEditVM");
        }
    }
}
