namespace HTM_Teste_Fazenda.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Refresh_Base : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FazendaNewEditVM", "Talhoes_Id", "dbo.TalhaoNewEditVM");
            DropIndex("dbo.FazendaNewEditVM", new[] { "Talhoes_Id" });
            DropColumn("dbo.FazendaNewEditVM", "Talhoes_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FazendaNewEditVM", "Talhoes_Id", c => c.Int());
            CreateIndex("dbo.FazendaNewEditVM", "Talhoes_Id");
            AddForeignKey("dbo.FazendaNewEditVM", "Talhoes_Id", "dbo.TalhaoNewEditVM", "Id");
        }
    }
}
