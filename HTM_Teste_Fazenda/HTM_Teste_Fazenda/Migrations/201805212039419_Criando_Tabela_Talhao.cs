namespace HTM_Teste_Fazenda.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Criando_Tabela_Talhao : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TalhaoNewEditVM",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                        IdFazenda = c.Int(nullable: false),
                        Ativo = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.FazendaNewEditVM", "Talhoes_Id", c => c.Int());
            AlterColumn("dbo.FazendaNewEditVM", "Ativo", c => c.Boolean());
            CreateIndex("dbo.FazendaNewEditVM", "Talhoes_Id");
            AddForeignKey("dbo.FazendaNewEditVM", "Talhoes_Id", "dbo.TalhaoNewEditVM", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FazendaNewEditVM", "Talhoes_Id", "dbo.TalhaoNewEditVM");
            DropIndex("dbo.FazendaNewEditVM", new[] { "Talhoes_Id" });
            AlterColumn("dbo.FazendaNewEditVM", "Ativo", c => c.Boolean(nullable: false));
            DropColumn("dbo.FazendaNewEditVM", "Talhoes_Id");
            DropTable("dbo.TalhaoNewEditVM");
        }
    }
}
