using HTM_Teste_Fazenda.Core;
using HTM_Teste_Fazenda.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace HTM_Teste_Fazenda.Controllers
{
  public class FazendaController : Controller
  {
    // GET: Fazenda
    public ActionResult Index()
    {
      return View();
    }

    public ActionResult NewEdit()
    {
      return View();
    }

    [HttpGet]
    public ActionResult NewEditJson(int? id)
    {
      FazendaNewEditVM fazendaVm = new FazendaNewEditVM();

      if (id.HasValue)
      {
        fazendaVm = new FazendaNewEditDb().ListarPorId(id.Value);
        fazendaVm.Talhoes = new TalhaoNewEditDb().ListarPorIdFazenda(id.Value);
      }

      return Json(fazendaVm, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public ActionResult List(ListVM listVM)
    {
      FazendaNewEditVM fazendaVM = new FazendaNewEditVM();
      fazendaVM.Descricao = listVM.Descricao;
      fazendaVM.Ativo = listVM.Ativo;

      List<FazendaNewEditVM> lstFazenda = new FazendaNewEditDb().ListarPorParametros(fazendaVM);

      return Json(new { Data = lstFazenda });
    }

    [HttpPost]
    public ActionResult NewEdit(FazendaNewEditVM fazendaVm)
    {
      bool sucesso = true;
      FazendaNewEditDb fazendaDb = new FazendaNewEditDb();
      TalhaoNewEditDb talhaoDb = new TalhaoNewEditDb();

      if (string.IsNullOrEmpty(fazendaVm.Descricao))
      {
        fazendaVm.Sucesso = false;
        fazendaVm.Mensagem = "Preencha o campo descrição.";
        return Json(new { Data = fazendaVm });
      }

      FazendaNewEditVM fazendaVMValidar = fazendaDb.ListarPorDescricao(fazendaVm.Descricao.TrimStart().TrimEnd());
      if (fazendaVMValidar != null && fazendaVMValidar.Id != fazendaVm.Id)
      {
        fazendaVm.Sucesso = false;
        fazendaVm.Mensagem = "Já existe uma fazenda com esta descrição.";
      }
      else
      {
        using (TransactionScope transacao = new TransactionScope())
        {
          sucesso = fazendaDb.Salvar(fazendaVm, string.Empty);
          if (sucesso)
          {
            if (fazendaVm.Talhoes != null)
            {
              foreach (var item in fazendaVm.Talhoes)
              {
                if (item != null)
                {
                  item.IdFazenda = fazendaVm.Id;

                  sucesso = talhaoDb.Salvar(item, string.Empty);
                  if (!sucesso)
                  {
                    fazendaVm.Sucesso = sucesso;
                    fazendaVm.Mensagem = "Erro ao salvar o Talhão.";
                    break;
                  }
                }
              }
            }

            List<TalhaoNewEditVM> lstTalhoes = new TalhaoNewEditDb().ListarPorIdFazenda(fazendaVm.Id);

            foreach (var item in lstTalhoes)
            {
              if (fazendaVm.Talhoes == null || !fazendaVm.Talhoes.ToList().Exists(p => p.Id == item.Id))
              {
                sucesso = talhaoDb.Excluir(item, string.Empty);
                if (!sucesso)
                {
                  fazendaVm.Sucesso = sucesso;
                  fazendaVm.Mensagem = "Erro ao excluir o Talhão.";
                  return Json(new { Data = fazendaVm });
                }
              }
            }
            if (sucesso)
            {
              fazendaVm.Sucesso = sucesso;
              transacao.Complete();
            }
          }
          else
          {
            fazendaVm.Sucesso = sucesso;
            fazendaVm.Mensagem = "Erro ao salvar a Fazenda.";
            return Json(new { Data = fazendaVm });
          }
        }
      }

      return Json(new { Data = fazendaVm });
    }
  }
}
