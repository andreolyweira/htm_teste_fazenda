var vm = {};

debugger;
vm.Descricao = ko.observable(null);
vm.Ativo = ko.observable(null);
vm.Data = ko.observableArray([]);

window.vm = vm;


vm.Pesquisar = function (data, event) {

  Pesquisar();
}

ko.applyBindings(vm, document.getElementById("frmListFazenda"))
Pesquisar();

function Pesquisar() {
  $.ajax({
    url: "List",
    type: "POST",
    data: { Descricao: vm.Descricao(), Ativo: vm.Ativo() },
    dataType: "json",
    async: false,
    success: function (response) {
      var responseVM = ko.mapping.fromJS(response.Data);
      vm.Data(responseVM());
    }

  });
}
