﻿var EntidadeTalhao = function () {
    this.Id = null;
    this.Descricao = null;
    this.Ativo = null;
};

getSimpleObjectByUrl = function (url, id) {

    var response = null;
    $.ajax({
        url: url,
        cache: false,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        async: false,
        data: (id !== undefined || id !== null) ? 'id=' + id : {},
        success: function (data) {
            response = data;
        }
    });

    return response;
}
var id = null;

var urlArgs = window.location.pathname.split('/');
if (urlArgs.length > 0 && urlArgs.length === 4) {
    id = urlArgs[3];
}

var vm = {};
var response = getSimpleObjectByUrl('/Fazenda/NewEditJson/', id);

vm = ko.mapping.fromJS(response);

if (vm.Talhoes == null || vm.Talhoes() == null) {
    vm.Talhoes = ko.observableArray([]);
}

var vmTalhao = function () {
    var self = this;
    self.Talhao = ko.mapping.fromJS(new EntidadeTalhao());
    self.TalhaoJs = ko.computed({
        read: function () {
            return ko.mapping.toJS(self.Talhao);
        },
        write: function (value) {
            return ko.mapping.fromJS(value, self.Talhao);
        },
        owner: this
    });

    self.IndexEdit = -1;

    self.Clear = function () {
        self.IndexEdit = -1;
        self.TalhaoJs(new EntidadeTalhao());
    };
    self.Clear();

    self.Save = function () {
        if (self.IndexEdit >= 0) {
            vm.Talhoes.remove(vm.Talhoes()[self.IndexEdit]);
        }

        //var existeDescricao = false;
        //var erro = false;

        //if (self.TalhaoJs().Descricao != null) {
        //    for (var i = 0; i < vm.Talhoes().length; i++) {
        //        debugger;
        //        if (typeof vm.Talhoes === 'object') {
        //            if (vm.Talhoes()[i].Descricao.toLowerCase() === self.TalhaoJs().Descricao.toLowerCase()) {
        //                existeDescricao = true;
        //                break;
        //            }
        //        }
        //        if (typeof vm.Talhoes() === 'object') {
        //            if (vm.Talhoes()[i].Descricao().toLowerCase() === self.TalhaoJs().Descricao.toLowerCase()) {
        //                existeDescricao = true;
        //                break;
        //            }
        //        }
        //    }
        //}
        //else {
        //    erro = true;
        //    alert("Preencha o campo descrição.");
        //}

        //if (!erro) {
        //    if (!existeDescricao) {
        vm.Talhoes.push(self.TalhaoJs());

        self.Clear();

        $('#frmModal').modal('hide');
        //    }
        //    else {
        //        alert("Já existe um talhão com esta descrição para esta fazenda.");
        //    }
        //}
    };

    self.New = function () {
        self.Clear();
        $('#frmModal').modal();
    };

    self.Edit = function (idx) {
        self.IndexEdit = idx;
        self.TalhaoJs(vm.Talhoes()[idx]);
        $('#frmModal').modal();
    }

    self.Remove = function (idx) {
        vm.Talhoes.remove(vm.Talhoes()[idx]);
    }
};

vm.Salvar = function (data, event) {

    $.ajax({
        url: "NewEdit",
        type: "POST",
        data: ko.mapping.toJS(vm),
        dataType: "json",
        async: false,
        success: function (response) {
            if (response.Data.Sucesso === true) {
                alert('Registro salvo com sucesso');
                window.location.href = '/Fazenda/Index';
            }
            else {
                alert(response.Data.Mensagem);
            }
        }

    });
}

vm.Cancelar = function (data, event) {

    window.location.href = '/Fazenda/Index';
}

window.vm = vm;

var koTalhao = new vmTalhao();

ko.applyBindings(koTalhao, document.getElementById("_formModal"));
ko.applyBindings(vm, document.getElementById("frmFazenda"))


