﻿using HTM_Teste_Fazenda.Context;
using HTM_Teste_Fazenda.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data.SqlClient;

namespace HTM_Teste_Fazenda.Core
{
    public class TalhaoNewEditDb : BaseDb
    {
        string cmdSqlListarPorId = @"
            select *
              from TalhaoNewEditVM f
             where f.Id = @Id";

        string cmdSqlListarPorIdFazenda = @"
            select *
              from TalhaoNewEditVM f
             where f.IdFazenda = @IdFazenda";

        string cmdSqlListarPorParametros = @"
            select *
              from TalhaoNewEditVM f
             where (f.Descricao like @Descricao or @Descricao is null)
               and (f.Ativo = @Ativo or @Ativo is null)
             order by f.Descricao";

        string cmdSqlListarPorDescricao = @"
            select *
              from TalhaoNewEditVM f
             where f.Descricao = @Descricao";

        string cmdSqlExcluirPorIdFazenda = @"
            delete from TalhaoNewEditVM where IdFazenda = @IdFazenda";

        public bool Salvar(TalhaoNewEditVM info, string usuarioLogado)
        {
            if (info.Id <= 0)
                return Inserir<TalhaoNewEditVM>(info, usuarioLogado);
            else
                return Editar<TalhaoNewEditVM>(info, usuarioLogado);
        }
        public bool Excluir(TalhaoNewEditVM info, string usuarioLogado)
        {
            return Excluir<TalhaoNewEditVM>(info, usuarioLogado);
        }
        public bool ExcluirPorIdFazenda(int idFazenda)
        {
            try
            {
                using (this.Connection = new SqlConnection(this.StrConexao))
                {
                    this.OpenConnection();
                    return this.Connection.Execute(cmdSqlExcluirPorIdFazenda, new { IdFazenda = idFazenda }) > 0;
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                Connection.Close();
            }
        }

        public List<TalhaoNewEditVM> ListarPorParametros(TalhaoNewEditVM model)
        {
            List<TalhaoNewEditVM> lst = new List<TalhaoNewEditVM>();

            try
            {
                using (this.Connection = new SqlConnection(this.StrConexao))
                {
                    this.OpenConnection();
                    lst = this.Connection.Query<TalhaoNewEditVM>(cmdSqlListarPorParametros,
                        new
                        {
                            Descricao = string.Format("{0}{1}{2}", "%", model.Descricao, "%"),
                            Ativo = model.Ativo
                        }).ToList();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                Connection.Close();
            }
            return lst;
        }
        public List<TalhaoNewEditVM> ListarPorIdFazenda(int idFazenda)
        {
            List<TalhaoNewEditVM> lst = new List<TalhaoNewEditVM>();

            try
            {
                using (this.Connection = new SqlConnection(this.StrConexao))
                {
                    this.OpenConnection();
                    lst = this.Connection.Query<TalhaoNewEditVM>(cmdSqlListarPorIdFazenda,
                        new
                        {
                            IdFazenda = idFazenda
                        }).ToList();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                Connection.Close();
            }
            return lst;
        }

        public TalhaoNewEditVM ListarPorId(int id)
        {
            TalhaoNewEditVM obj = new TalhaoNewEditVM();

            try
            {
                using (this.Connection = new SqlConnection(this.StrConexao))
                {
                    this.OpenConnection();
                    obj = this.Connection.Query<TalhaoNewEditVM>(cmdSqlListarPorId, new { Id = id }).SingleOrDefault();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                Connection.Close();
            }
            return obj;
        }
        public TalhaoNewEditVM ListarPorDescricao(string descricao)
        {
            TalhaoNewEditVM obj = new TalhaoNewEditVM();

            try
            {
                using (this.Connection = new SqlConnection(this.StrConexao))
                {
                    this.OpenConnection();
                    obj = this.Connection.Query<TalhaoNewEditVM>(cmdSqlListarPorDescricao, new { Descricao = descricao }).SingleOrDefault();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                Connection.Close();
            }
            return obj;
        }
    }
}