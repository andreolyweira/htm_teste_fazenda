﻿using HTM_Teste_Fazenda.Context;
using HTM_Teste_Fazenda.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;

namespace HTM_Teste_Fazenda.Core
{
    public class FazendaNewEditDb : BaseDb
    {
        string cmdSqlListarPorId = @"
            select *
              from FazendaNewEditVM f
             where f.Id = @Id";

        string cmdSqlListarPorParametros = @"
            select f.*, count(t.Id) QtdeTalhoes
              from FazendaNewEditVM f
              left join TalhaoNewEditVM t on t.IdFazenda = f.Id
             where (f.Descricao like @Descricao or @Descricao is null)
               and (f.Ativo = @Ativo or @Ativo is null)
             group by f.Id,f.Descricao,f.Sigla,f.IdProprietario,f.IdMunicipio,f.Ativo
             order by f.Descricao";

        string cmdSqlListarPorDescricao = @"
            select *
              from FazendaNewEditVM f
             where f.Descricao = @Descricao";

        public bool Salvar(FazendaNewEditVM info, string usuarioLogado)
        {
            if (info.Id <= 0)
                return Inserir<FazendaNewEditVM>(info, usuarioLogado);
            else
                return Editar<FazendaNewEditVM>(info, usuarioLogado);
        }
        public bool Excluir(FazendaNewEditVM info, string usuarioLogado)
        {
            return Excluir<FazendaNewEditVM>(info, usuarioLogado);
        }

        public List<FazendaNewEditVM> ListarPorParametros(FazendaNewEditVM model)
        {
            List<FazendaNewEditVM> lst = new List<FazendaNewEditVM>();

            try
            {
                using (this.Connection = new SqlConnection(this.StrConexao))
                {
                    this.OpenConnection();
                    lst = this.Connection.Query<FazendaNewEditVM>(cmdSqlListarPorParametros,
                        new
                        {
                            Descricao = string.Format("{0}{1}{2}", "%", model.Descricao, "%"),
                            Ativo = model.Ativo
                        }).ToList();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                Connection.Close();
            }
            return lst;
        }

        public FazendaNewEditVM ListarPorId(int id)
        {
            FazendaNewEditVM obj = new FazendaNewEditVM();

            try
            {
                using (this.Connection = new SqlConnection(this.StrConexao))
                {
                    this.OpenConnection();
                    obj = this.Connection.Query<FazendaNewEditVM>(cmdSqlListarPorId, new { Id = id }).SingleOrDefault();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                Connection.Close();
            }
            return obj;
        }
        public FazendaNewEditVM ListarPorDescricao(string descricao)
        {
            FazendaNewEditVM obj = new FazendaNewEditVM();

            try
            {
                using (this.Connection = new SqlConnection(this.StrConexao))
                {
                    this.OpenConnection();
                    obj = this.Connection.Query<FazendaNewEditVM>(cmdSqlListarPorDescricao, new { Descricao = descricao }).SingleOrDefault();
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                Connection.Close();
            }
            return obj;
        }
    }
}