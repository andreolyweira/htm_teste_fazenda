﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HTM_Teste_Fazenda.ViewModel
{
    [Table("TalhaoNewEditVM")]
    public class TalhaoNewEditVM : BaseVM
    {
        public TalhaoNewEditVM() { }

        public int Id { get; set; }
        public string Descricao { get; set; }
        public int IdFazenda { get; set; }
        public bool? Ativo { get; set; }
    }
}