using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM_Teste_Fazenda.ViewModel
{
  public class ListVM
  {
    public ListVM()
    {
      Status = 2;
    }

    public string Descricao { get; set; }
    public bool? Ativo { get; set; }
    public int? Status { get; set; }
  }
}
