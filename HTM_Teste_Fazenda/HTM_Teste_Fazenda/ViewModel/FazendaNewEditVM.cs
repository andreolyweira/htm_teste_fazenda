﻿using HTM_Teste_Fazenda.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HTM_Teste_Fazenda.ViewModel
{
    [Table("FazendaNewEditVM")]
    public class FazendaNewEditVM : BaseVM
    {
        public FazendaNewEditVM()
        {
        }

        public int Id { get; set; }
        public string Descricao { get; set; }
        public string Sigla { get; set; }
        public int IdProprietario { get; set; }
        public int IdMunicipio { get; set; }
        public bool? Ativo { get; set; }

        [DapperIgnore]
        [NotMapped]
        public int QtdeTalhoes { get; set; }

        [DapperIgnore]
        [NotMapped]
        public string AtivoView
        {
            get
            {
                if (Ativo.HasValue)
                    return Ativo.Value ? "Sim" : "Não";
                else
                    return string.Empty;
            }
        }

        [DapperIgnore]
        [NotMapped]
        public string Detalhes
        {
            get
            {
                string proprietario = string.Empty;
                string municipio = string.Empty;

                switch (IdProprietario)
                {
                    case 1:
                        proprietario = "Humberto Meirelles";
                        break;
                    case 2:
                        proprietario = "Everton França";
                        break;
                    case 3:
                        proprietario = "André Oliveira";
                        break;
                }

                switch (IdMunicipio)
                {
                    case 1:
                        municipio = "Ribeirão Preto (SP)";
                        break;
                    case 2:
                        municipio = "Batatais (SP)";
                        break;
                    case 3:
                        municipio = "Cravinhos (SP)";
                        break;
                }

                return proprietario + " - " + municipio;
            }
        }

        [DapperIgnore]
        [NotMapped]
        public ICollection<TalhaoNewEditVM> Talhoes { get; set; }
    }
}