﻿using HTM_Teste_Fazenda.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HTM_Teste_Fazenda.ViewModel
{
    public class BaseVM
    {
        [DapperIgnore]
        [NotMapped]
        public bool Sucesso { get; set; }
        [DapperIgnore]
        [NotMapped]
        public string Mensagem { get; set; }
    }
}