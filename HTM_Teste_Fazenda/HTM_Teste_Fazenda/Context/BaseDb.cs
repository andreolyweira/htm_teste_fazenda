using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;

namespace HTM_Teste_Fazenda.Context
{
  [AttributeUsage(AttributeTargets.Property)]
  public class DapperKey : Attribute
  {
  }

  [AttributeUsage(AttributeTargets.Property)]
  public class DapperIgnore : Attribute
  {
  }
  [AttributeUsage(AttributeTargets.Property)]
  public class DapperAllowNull : Attribute
  {

  }

  public class BaseDb
  {
    public BaseDb()
    {
      this.StrConexao = strConexao;
    }
    string strConexao = ConfigurationManager.ConnectionStrings["HTMFazenda"].ConnectionString;

    public SqlCommand Sql { get; set; }

    public SqlConnection Connection { get; set; }

    public string StrConexao { get; set; }
    public void OpenConnection()
    {
      try
      {
        if (this.Connection.State != ConnectionState.Open)
          this.Connection.Open();
      }
      catch (Exception ex)
      {
        throw new Exception("Não foi possível estabelecer conexão com o banco de dados. Tente novamente mais tarde. " + ex.Message);
      }
    }


    protected SqlConnection GetOpenConnection()
    {
      var connection = new SqlConnection(strConexao);
      connection.Open();
      return connection;
    }

    protected int Execute(CommandType commandType, string sql, object parameters = null)
    {
      using (var connection = GetOpenConnection())
      {
        return connection.Execute(sql, parameters, commandType: commandType);
      }
    }
    protected IEnumerable<T> GetItems<T>(CommandType commandType, string sql, object parameters = null)
    {
      using (var connection = GetOpenConnection())
      {
        return connection.Query<T>(sql, parameters, commandType: commandType);
      }
    }

    protected bool Inserir<T>(T obj, string usuarioLogado)
    {
      var propertyContainer = ParseProperties(obj, usuarioLogado, true);
      var sql = string.Format("INSERT INTO {0} ({1}) VALUES(@{2}); SELECT @@IDENTITY;",
          typeof(T).Name.Replace("Model", ""),
          string.Join(", ", propertyContainer.ValueNames),
          string.Join(", @", propertyContainer.ValueNames));

      using (var connection = GetOpenConnection())
      {
        var id = connection.Query<int>(sql, propertyContainer.ValuePairs, commandType: CommandType.Text).First();

        SetId(obj, id, propertyContainer.IdPairs);

        return id > 0;
      }
    }

    protected bool Editar<T>(T obj, string usuarioLogado)
    {
      var propertyContainer = ParseProperties(obj, usuarioLogado, false);
      var sqlIdPairs = GetSqlPairs(propertyContainer.IdNames);
      var sqlValuePairs = GetSqlPairs(propertyContainer.ValueNames);
      var sql = string.Format("UPDATE {0} SET {1} WHERE {2}", typeof(T).Name.Replace("Model", ""), sqlValuePairs, sqlIdPairs);
      return Execute(CommandType.Text, sql, propertyContainer.AllPairs) > 0;
    }

    protected bool Excluir<T>(T obj, string usuarioLogado)
    {
      var propertyContainer = ParseProperties(obj, usuarioLogado, false);
      var sqlIdPairs = GetSqlPairs(propertyContainer.IdNames);
      var sql = string.Format("DELETE FROM {0} WHERE {1}", typeof(T).Name.Replace("Model", ""), sqlIdPairs);
      return Execute(CommandType.Text, sql, propertyContainer.IdPairs) > 0;
    }

    private static string GetSqlPairs(IEnumerable<string> keys, string separator = ", ")
    {
      var pairs = keys.Select(key => string.Format("{0}=@{0}", key)).ToList();
      return string.Join(separator, pairs);
    }
    private void SetId<T>(T obj, int id, IDictionary<string, object> propertyPairs)
    {
      if (propertyPairs.Count == 1)
      {
        var propertyName = propertyPairs.Keys.First();
        var propertyInfo = obj.GetType().GetProperty(propertyName);
        if (propertyInfo.PropertyType == typeof(int))
        {
          propertyInfo.SetValue(obj, id, null);
        }


      }
    }
    private static PropertyContainer ParseProperties<T>(T obj, string usuarioLogado, bool insert = true)
    {
      var propertyContainer = new PropertyContainer();

      var typeName = typeof(T).Name.Replace("Model", "");
      var validKeyNames = new[] { "Id",
            string.Format("{0}Id", typeName), string.Format("{0}_Id", typeName) };

      var properties = typeof(T).GetProperties();
      foreach (var property in properties)
      {
        // Skip reference types (but still include string!)
        if (property.PropertyType.IsClass && property.PropertyType != typeof(string))
          continue;

        // Skip methods without a public setter
        if (property.GetSetMethod() == null)
          continue;

        // Skip methods specifically ignored
        if (property.IsDefined(typeof(DapperIgnore), false))
          continue;

        var name = property.Name;
        var value = typeof(T).GetProperty(property.Name).GetValue(obj, null);

        if (property.IsDefined(typeof(DapperAllowNull), false) && value != null && value.ToString() == "0")
        {
          value = null;
        }

        if (property.IsDefined(typeof(DapperKey), false) || validKeyNames.Contains(name))
        {
          propertyContainer.AddId(name, value);
        }
        else
        {
          if (insert)
          {
            if (name.Equals("DataCriacao"))
              value = DateTime.Now;
            else if (name.Equals("UsuarioCriacao"))
              value = usuarioLogado;
          }
          else if (!insert)
          {
            if (name.Equals("DataAlteracao"))
              value = DateTime.Now;
            else if (name.Equals("UsuarioAlteracao"))
              value = usuarioLogado;
          }
          propertyContainer.AddValue(name, value);
        }
      }

      return propertyContainer;
    }

    private class PropertyContainer
    {
      private readonly Dictionary<string, object> _ids;
      private readonly Dictionary<string, object> _values;

      #region Properties

      internal IEnumerable<string> IdNames
      {
        get { return _ids.Keys; }
      }

      internal IEnumerable<string> ValueNames
      {
        get { return _values.Keys; }
      }

      internal IEnumerable<string> AllNames
      {
        get { return _ids.Keys.Union(_values.Keys); }
      }

      internal IDictionary<string, object> IdPairs
      {
        get { return _ids; }
      }

      internal IDictionary<string, object> ValuePairs
      {
        get { return _values; }
      }

      internal IEnumerable<KeyValuePair<string, object>> AllPairs
      {
        get { return _ids.Concat(_values); }
      }

      #endregion

      #region Constructor

      internal PropertyContainer()
      {
        _ids = new Dictionary<string, object>();
        _values = new Dictionary<string, object>();
      }

      #endregion

      #region Methods

      internal void AddId(string name, object value)
      {
        _ids.Add(name, value);
      }

      internal void AddValue(string name, object value)
      {
        _values.Add(name, value);
      }

      #endregion
    }
  }
}
