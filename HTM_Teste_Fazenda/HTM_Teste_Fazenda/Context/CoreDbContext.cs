﻿using HTM_Teste_Fazenda.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HTM_Teste_Fazenda.Context
{
    public class CoreDbContext : DbContext
    {
        public CoreDbContext() : base("HTMFazenda") { }

        DbSet<FazendaNewEditVM> Fazendas { get; set; }
        DbSet<TalhaoNewEditVM> Talhoes { get; set; }
    }
}