﻿using System.Web;
using System.Web.Mvc;

namespace HTM_Teste_Fazenda
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
